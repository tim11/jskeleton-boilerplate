'use strict';
const
    ncp = require("ncp"),
    path = require("path"),
    fs = require("fs"),
    shell = require('shelljs'),
    gulp = require("./system/task/gulpfile"),
    merge = require("deepmerge"),
    globalConfig = require("./system/config/base");


let 
    argv = process.argv,
    currentDir = process.cwd(),
    config = globalConfig;
const Boilerplate = function() {
    if (fs.existsSync(path.join(currentDir, "/config.js"))) {
        var userConfig = require(path.join(currentDir, "/config.js"));
        config = merge(config, userConfig);
    }
    
    const getCmdParam = (need, def = null) => {
        if (need !== undefined) {
            let regexp = new RegExp('--' + need + '=(.+)', 'i');
            argv.some(key => {
                let parse = regexp.exec(key);
                if (parse) {
                    def = parse[1];
                    return true;
                }
            });
        }
        return def;
    }

    const template = (dist) => {

        if (!dist) {
            dist = self.getCmdParam('dist')
        }
        if (dist) {
            var folder = path.join(currentDir, dist);
            try {
                fs.mkdirSync(folder);
            } catch (err) {
                if (err.code !== "EEXIST") return;
            }
            ncp(path.join(__dirname, "./system/boilerplate/"), folder);
        }
    }

    const generateSmacsFile = (type, name) => {
        switch (type) {
            case "base":
            case "section":
            case "block":
            case "utils":
            case "element":
                var folder = path.join(currentDir, config.scss.input.dir, '/', type);
                if (shell.mkdir('-p', folder).code !== 0) {
                    shell.echo("Can't create directory " + folder);
                    shell.exit(1);
                } else {
                    let cname = name.replace(/[^\d\w_-]/gim, '');
                    if (!cname.length) {
                        console.log('Invalid name + ' + name);
                    } else {
                        var file = path.join(folder, '/_' + cname + '.scss');
                        if (!fs.existsSync(file)) {
                            var text = "";
                            if (type == "block") {
                                text += ".block-" + cname + "{\r\n}";
                            }
                            if (type == "section") {
                                text += "#section-" + cname + "{\r\n}";
                            }
                            fs.writeFileSync(file, text);
                            var indexFile = path.join(folder, '/index.scss');
                            text = "@import '_" + cname + "';";
                            if (!fs.existsSync(indexFile)) {
                                fs.writeFileSync(indexFile, text);
                            } else {
                                fs.appendFileSync(indexFile, `\n${text}`);
                            }
                        }
                    }
                }
                break;
            default:
                console.log('Undefined type + ' + type);
                break;
        }
    }
    
    config.mode = getCmdParam("mode", "standart");
    gulp.setSettings(config);

    let action = getCmdParam('action');
    switch (action) {
        case 'template':
            console.log('Generate boilerplate');
            template(config.dirs.source);
            break;
        case 'start':
            gulp.run('start-server')
            break;
        case 'compress':
            console.log('Comperss files');
            gulp.run('compress')
            break;
        case 'plugin-add':
            var pkg = getCmdParam('name');
            if (pkg !== null) {
                console.log('Install package ' + pkg + '...');
                var folder = path.join(currentDir, config.dirs.assets);
                if (shell.mkdir('-p', folder).code !== 0) {
                    shell.echo("Can't create directory " + folder);
                    shell.exit(1);
                }
                var file = path.join(folder, '/package.json');
                if (!fs.existsSync(file)) {
                    fs.writeFileSync(file, '{}');
                }
                if (shell.exec('npm install --save --prefix ' + folder + ' ' + pkg).code !== 0) {
                    shell.echo('We can not install package ' + pkg);
                    shell.exit(1);
                }
                console.log('Install ' + pkg + ' is complete')
            } else {
                console.log('Please, set package name');
            }
            break;
        case 'plugin-remove':
            var pkg = getCmdParam('name');
            if (pkg !== null) {
                console.log('Uninstall package ' + pkg + ' ...');
                var folder = path.join(currentDir, config.dirs.assets);
                if (shell.exec('npm remove --prefix ' + folder + ' ' + pkg).code !== 0) {
                    shell.echo('We can not uninstall package ' + 'pkg');
                    shell.exit(1);
                }
                console.log('Uninstall ' + pkg + ' is complete')
            } else {
                console.log('Please, set package name');
            }
            break;
        case 'gen-base':
            var name = getCmdParam('name');
            if (name !== null) {
                console.log('Generate SMACS base file ' + name);
                generateSmacsFile('base', name);
            } else {
                console.log('Please, set SMACS base name');
            }
            break;
        case 'gen-element':
            var name = self.getCmdParam('name');
            if (name !== null) {
                console.log('Generate SMACS element file ' + name);
                generateSmacsFile('element', name);
            } else {
                console.log('Please, set SMACS element name');
            }
            break;
        case 'gen-utils':
            var name = self.getCmdParam('name');
            if (name !== null) {
                console.log('Generate SMACS utils file ' + name);
                generateSmacsFile('utils', name);
            } else {
                console.log('Please, set SMACS utils name');
            }
            break;
        case 'gen-block':
            var name = self.getCmdParam('name');
            if (name !== null) {
                console.log('Generate SMACS block file ' + name);
                generateSmacsFile('block', name);
            } else {
                console.log('Please, set SMACS block name');
            }
            break;
        case 'gen-section':
            var name = self.getCmdParam('name');
            if (name !== null) {
                console.log('Generate SMACS section file ' + name);
                generateSmacsFile('section', name);
            } else {
                console.log('Please, set SMACS section name');
            }
            break;
        default:
            console.log("Unknown command " + action);
            break;
    }
}

var inst = new Boilerplate();
module.exports = inst;