# jskeleton-boilerplate
### Установка пакета 
```bash
    npm install jskeleton-boilerplate
```
### Установка базового шаблона
```bash
    boilerplate --action=template
```
Если хотите равзернуть проект в другой папке, можно указать параметр **dist**
```bash
    boilerplate --action=template --dist='путь_к_папке'
```
Также система поддерживает формат верстки электронного письма с помощью параметра **mode** со значением **email**. В таком режиме все стили будут прописаны прямо в html верстке
```bash
    boilerplate --action=template --mode=email
```
### Запуск проекта
```bash
    boilerplate --action=start
```
### Создания минифицированных файлов
```bash
    boilerplate --action=compress
```
### Установка js плагина
```bash
    boilerplate --action=plugin-add --name='имя_плагина'
```
### Удаление js плагина
```bash
    boilerplate --action=plugin-remove --name='имя_плагина'
```
### Создание элементов SMACS разметки
```bash
    boilerplate --action=gen-'тип' --name='имя'
```
Типы бывают нескольких видов
* **base**
Создает файл в папке **base** в корневой директории **sass** файлов
* **element**
Создает файл в папке **element** в корневой директории **sass** файлов
* **utils**
Создает файл в папке **utils** в корневой директории **sass** файлов
* **block**
Создает файл в папке **block** в корневой директории **sass** файлов
* **section**
Создает файл в папке **section** в корневой директории **sass** файлов

Например:
```bash
    boilerplate --action=gen-section --name=header
```
### Конфигурация
Файл **config.js** располагается в корне Вашего проекта.
Состав:
```js
const config = {
    dirs: {
        public: "./public", //Путь до папки, куда будут собираться скомпилированные файлы и откуда запуститься браузер
        source: "./src", // Папка с исходниками
        assets: "./public/assets" //Папка для инсталяции пользовательских плагинов
    },
    scss: {
        input: {
            dir:  "./src/scss", // Папка с исходниками SASS файлов
            file: "./src/scss/index.scss" // Индексный файл для компиляции sass
        },
        output: {
            dir:  "./public/css", // Папка для публикации CSS
            file: "./public/css/style", //Скомпилированный CSS
            min:  "./public/css/style.min" //Минифицированный CSS
        }
    },
    html: {
        input: {
            dir:  "./src/html" // Папка с исходниками HTML
        },
        output: {
            dir: "./public" // Папка публикации скомпилированного HTML
        }
    },
    js: {
        input: {
            dir:  "./src/js", // Папка с исходниками JS файлов
            file: "./src/js/index.js" // Индексный файл для компиляции JS
        },
        output: {
            dir:  "./public/js", // Папка публикации скомпилированного JS
            file: "./public/js/index", //Скомпилированный JS
            min:  "./public/js/index.min" //Минифицированный JS
        }
    }
}
module.exports = config; //Обязательно экспортируйте конфиг
```