const merge = require('deepmerge');

let config = {
    mode: "standart",
    dirs: {
        public: "./public",
        source: "./src",
    }
}
config = merge(config, {
    dirs: {
        assets: config.dirs.public + "/assets"
    },
    scss: {
        input: {
            dir: config.dirs.source + "/scss",
            file: config.dirs.source + "/scss/index.scss"
        },
        output: {
            dir: config.dirs.public + "/css",
            file: config.dirs.public + "/css/style",
            min: config.dirs.public + "/css/style.min"
        }
    },
    html: {
        input: {
            dir: config.dirs.source + "/html"
        },
        output: {
            dir: config.dirs.public
        }
    },
    js: {
        input: {
            dir: config.dirs.source + "/js",
            file: config.dirs.source + "/js/index.js"
        },
        output: {
            dir: config.dirs.public + "/js",
            file: config.dirs.public + "/js/index",
            min: config.dirs.public + "/js/index.min"
        }
    }
});

module.exports = config;