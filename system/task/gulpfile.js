'use strict';
const gulp = require("gulp")

const browserSync = require('browser-sync').create();
const sass = require("gulp-sass");
const uglify = require("gulp-uglify");
const rename = require("gulp-rename");
const cleanCSS = require('gulp-clean-css');
const rigger = require('gulp-rigger');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const uncss = require('gulp-uncss');
const uglifycss = require('gulp-uglifycss');
const path = require('path');
const inlineCss = require('gulp-inline-css');
const watch = require('gulp-watch');

var settings = require('../config/base');

var
    compress = false,
    production = false;

gulp.task('start-server', function() {

    watch([
        settings.html.input.dir + "/*.html",
        settings.html.input.dir + "/**/*.html"
    ], {
        events: ['add', 'change']
    }, function() {
        gulp.run('html');
    });
    watch([
        settings.scss.input.dir + "/*.scss",
        settings.scss.input.dir + "/**/*.scss"
    ], {
        events: ['add', 'change']
    }, function() {
        gulp.run('sass');
    });
    watch([
        settings.js.input.file
    ], {
        events: ['add', 'change']
    }, function() {
        gulp.run('js');
    });
    browserSync.init({
        server: settings.dirs.public,
        port: 4000
    });
});

gulp.task("sass", function() {
    var file = gulp
        .src(settings.scss.input.file)
        .pipe(plumber())
        .pipe(
            sass().on('error', sass.logError)
        )
        .pipe(
            autoprefixer({
                browsers: ["last 15 versions", "> 1%", "ie 9"],
            })
        )
        .pipe(
            rename({
                basename: path.basename(settings.scss.output.file)
            })
        )
        .pipe(gulp.dest(settings.scss.output.dir));
    if (compress) {
        file
            .pipe(
                cleanCSS({
                    compatibility: 'ie8'
                })
            )
            .pipe(
                rename({
                    basename: path.basename(settings.scss.output.min)
                })
            )
            .pipe(gulp.dest(settings.scss.output.dir));
    }
    if (production) {
        file
            .pipe(uncss({
                html: [
                    settings.html.input.dir + "/*.html",
                    settings.html.input.dir + "/**/*.html"
                ]
            }))
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(
                rename({
                    basename: path.basename(settings.scss.output.min)
                })
            )
            .pipe(
                gulp.dest(settings.scss.output.dir)
            );
    }

    if (settings.mode && settings.mode == "email") {
        gulp.run('html');
    } else {
        console.log('Update style');
        file
            .pipe(
                browserSync.stream({
                    // once: true
                })
            );
    }
});

gulp.task("js", function() {
    var file = gulp
        .src(settings.js.input.file)
        .pipe(plumber())
        .pipe(
            rename({
                basename: path.basename(settings.js.output.file)
            })
        )
        .pipe(gulp.dest(settings.js.output.dir));
    if (compress) {
        file
            .pipe(uglify({
                mangle: true
            }))
            .pipe(
                rename({
                    basename: path.basename(settings.js.output.min)
                })
            )
            .pipe(gulp.dest(settings.js.output.dir));
    }
    if (browserSync)
        browserSync.reload();
});

gulp.task("html", function() {
    var file = gulp
        .src(settings.html.input.dir + '/*.html')
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest(settings.dirs.public));
    if (settings.mode && settings.mode == "email") {
        file
            .pipe(inlineCss())
            .pipe(gulp.dest(settings.html.output.dir));
    }
    if (browserSync)
        browserSync.reload();

});

gulp.task("compress", function() {
    production = true;
    compress = true;
    gulp.run("sass");
    gulp.run("js");
    gulp.run("html");
})

gulp.task('start-server', gulp.series('sass', 'js', 'html'));


const setSettings = (config) => {
    settings = config;
}

module.exports = {
    gulp: gulp,
    setSettings: setSettings
}